//
//  section.h
//  Multisymplectic_GS
//
//  Created by Nader on 09/03/2017.
//  Copyright © 2017 Nader Ganaba. All rights reserved.
//

#ifndef section_h
#define section_h





#define _USE_MATH_DEFINES
#include<cmath>
#include <complex>
#include<iostream>
#include<algorithm>
#include<iomanip>
#include<fstream>
#include<sstream>
#include<string>
#include<cstring>
#include<cctype>
#include<vector>
#include "basespace.h"


#define u_vec(i)   (z[ i])
#define ell_vec(i) (z[(nx + 2)  + i])
#define pi_vec(i)  (z[2*(nx + 2)+ i])
#define POW3(x) (x*x*x)
#define POW2(x) (x*x)
#define XMAX 80.0
#define MAXITER 1000

class Section_CH{
private:
    double* u;
    double* ell;
    double* pi_var;
    double* z; //Used for Gauss-Seidel
    double* conserved_m;
    int nx , nt;
    int curr_iter;
    double dx, dt;
    double kappa, c;
    double eps1 , eps2;
    double omega;
    //double* v;
    //double* w;
    //Add traits
public:
    Section_CH(int _nx, int _nt){
        nx = _nx; nt = _nt;  dx = (XMAX / (double)(nx));
        //dt = (1.0 / (double)(nt - 1));
         eps1 = 1e-10;
         eps2 = 1e-10;
        omega =1.55;
        dt = 0.01;
        kappa = 0.75; c = 3;
        curr_iter =0;
        std::cout << "dx = " << dx << "\t dt = " << dt << std::endl;
        u = (double*) calloc ((nx+2)*nt,sizeof(double));
        ell = (double*) calloc ((nx+2)*nt,sizeof(double));
        pi_var = (double*) calloc ((nx+2)*nt,sizeof(double));
        z = (double*) calloc (((nx+2)*3),sizeof(double));
        conserved_m =(double*) calloc (5*nt,sizeof(double));
    }
    
    
    void set_curr_iter(int n) {curr_iter = n;}
    int get_curr_iter(){return curr_iter;}
    
    void set_tolerance(double tol){eps1 = tol;}
    
    void set_init_cond(){ //Needs changing
        
        std::ifstream File_ell;
        File_ell.open("init_ell_512.txt");
        
        int idx = 0;
        while(!File_ell.eof())
        {
            File_ell >> ell[idx];
            idx++;
        }
        
        File_ell.close();
        
        std::ifstream File_pi;
        File_pi.open("init_P_512.txt");
        
        idx = 0;
        while(!File_pi.eof())
        {
            File_pi >> pi_var[idx];
            idx++;
        }
        
        File_pi.close();
        
        std::ifstream File_u;
        File_u.open("init_U_512.txt");
        
        idx = 0;
        while(!File_u.eof())
        {
            File_u >> u[idx];
            idx++;
        }
        
        File_u.close();
        
        //        std::cout << "U \t \ell \t \pi" << std::endl;
        //        for(int i = 0; i < nx+2; i++){
        //
        //            std::cout << u[i] << "\t\t" << ell[i] << "\t\t" << pi_var[i] << std::endl;
        //        }
        
        
    }
    
    void comp_conserved_m(int n){
        
        double temp = 0.0;
        double temp2 = 0.0;
        double dx2 = 1.0/(dx*dx);
        int idx;
        idx = n*nx + 0;
        //temp +=  ((u[idx ] - dx2*(u[idx+1] - 2*u[idx] + u[n*nx + nx])));
        
        for(int i = 1; i <= nx; i++){
            idx = n*(nx+2) + i;
            temp = (u[idx] - dx2*(u[idx+1] - 2*u[idx] + u[idx-1]));
            //Casimir  sqrt(m)
            conserved_m[n] +=   sqrt(temp  + kappa)*dx;
            
            //Hamiltonian : 1/2 \int m u dx
            conserved_m[nt + n] += 0.5*dx*((temp + kappa)*u[idx] );
            
            //\int m dx
            conserved_m[2*nt + n] += dx*(temp + kappa);
            
            //1/2 \int u^3 + u (u_x)^2 dx
            temp2 = POW3(u[idx]) + (1.0/(2.0*dx))*u[idx]*(u[idx+1] - u[idx-1]);
            conserved_m[3*nt + n] += dx*0.5*temp2 ;
            
            //\int p dx
            conserved_m[4*nt + n] += dx*(pi_var[idx]);
        }
        
        //temp +=  ((u[idx ] - dx2*(u[0] - 2*u[idx] + u[idx - 1])));
        
        // conserved_m[n] = temp;
        
    }
    
    //for debug
    
    void print_u(int n){
        int idx;
        std::cout << "numerical \t| \t exact " << std::endl;
        double sol;
        
        for(int i = 1; i <=nx ; i++){
            idx = n*(nx+2) + i;
            
            std::cout << u[idx]  << std::endl;
        }
    }
    
    void print_l(int n){
        int idx;
        std::cout << "Ell " << std::endl;
        double sol;
        
        for(int i = 0; i < nx+1; i++){
            idx = n*(nx+2) + i;
            
            std::cout << ell[idx] << std::endl;
        }
    }
    
    section_params load_params(){
        
        section_params temp;
        
        temp.dt = dt;
        temp.dx = dx;
        temp.x_max = XMAX;
        temp.nt = nt;
        temp.nx = nx;
        
        return temp;
        
    }
    
    void print_vars(int n){
        int idx;
        std::cout << "U \t \ell \t \pi" << std::endl;
        
        
        for(int i = 0; i < nx+2; i++){
            idx = n*(nx + 2) + i;
            
            std::cout << u[idx] << "\t\t" << ell[idx] << "\t\t" << pi_var[idx] << std::endl;
        }
    }
    
    void print_c(){
        
        std::cout << "I_1 \t I_2 \t I_3 \t I_4 \t I_5" << std::endl;
        
        
        for(int i = 0; i < nt; i++){
            
            std::cout << conserved_m[i] << "\t";
            std::cout << conserved_m[i + nt] << "\t";
            std::cout << conserved_m[i + 2*nt] << "\t";
            std::cout << conserved_m[i + 3*nt] << "\t";
            std::cout << conserved_m[i + 4*nt] << std::endl;
        }
    }
    
    void export_u(int n, std::string filename){
        
        std::string _filename = "test_plot.dat";
        
        std::ofstream output_file (filename);
        
        int idx;
        std::cout << "Exporting" << std::endl;
        if(output_file.is_open()){
            for(int i = 1; i <= nx; i++){
                idx = n*(nx+2) + i;
                
                output_file <<  u[idx] << "\t\t, " << ell[idx] << "\t\t, " << pi_var[idx] << "\n";
                
            }
        }
        
        output_file.close();
    }
    
    //    template< class InitialCondition >
    //    void set_init_cond( InitialCondition &phi0,  BaseSpace &B ){
    //        int nx = B.get_num_x_points();
    //        for(int i = 0; i < nx; i++){
    //
    //            //u[i + nx] = phi0(B.get_location(0, i).x);
    //            //For testing only
    //            u[i] = phi0(B.get_location(0, i).x);
    //
    //        }
    //
    //        //Figure something for w and v
    //
    //
    //    }
    
    void Section_CH_at(int n ){
        
        int idx ;
        for(int i = 0; i < nx+2; i++){
            idx = n*(nx+2) + i;
            
            z[i] = u[idx];
        }
        
        
    }
    
    void Section_CH_full_at(int n ){
        int idx ;
        for(int i = 0; i < nx+2; i++){
            idx = n*(nx+2) + i;
            z[i] = u[idx];
        }
    }
    
    void Section_CH_guess(int n ){
        int idx ;
        for(int i = 0; i < nx+2; i++){
            idx = (n-1)*(nx+2) + i;

//            z[i] = u[idx];
//            z[(nx + 2)  + i] = ell[idx];
//            z[2*(nx + 2)  + i] = pi_var[idx];
            
            u_vec(i) = u[idx];
            ell_vec(i) = ell[idx];
            pi_vec(i) = pi_var[idx];

        }
    }
    
    //Diagnostics
    void print_z(int n){
        int idx;
        std::cout << "U \t \ell \t \pi" << std::endl;

        for(int i = 0; i < nx+2; i++){
            idx = n*(nx + 2) + i;
            
            std::cout << z[i] << "\t\t" << z[(nx + 2)  + i] << "\t\t" << z[2*(nx + 2)  + i] << std::endl;
        }
    }
    
    void update_Section_CH(int n  ){
        int idx = 0;
        
        for(int i = 0; i < nx+2; i++){
            idx = n*(nx+2) + i;
            
            u[idx] = u_vec(i);
            ell[idx] = ell_vec(i);
            pi_var[idx] = pi_vec(i);
            
        }
        
    }
    

    
    void eval_EL_CH( int n ){
        //int n = curr_iter;
        //idx1 = (n+1)*nx + i;
        //idx2 = (n)*nx + i;
        //idx3 = (n+1)*nx + i+1;
        //idx4 = (n)*nx + i + 1;
        
        //u_{n+1, i} = x0(i)
        //v_{n+1, i} = x0(nx + i)
        //w_{n+1, i} = x0(2*nx + i)
        
        //         std::complex<float> dt1(  1.0/ (2.0*dt), 0.0);
        //         std::complex<float> dx1( 1.0/ (2.0*dx), 0.0);
        //
        double dt1 = 1.0/ dt;
        double dx1 = 1.0/ dx;
        double dx2 = 1.0/ (dx*dx);
        double dxdt = dt / dx;
        int idx1, idxn, idx0;
        int idx2, idx2k;
        
        int idxu, idxl, idxpi;
        double a = 2.0;
        double one_over_four = 0.25;
        //idx3 = (n+1)*nx + i+1;
        //idx4 = (n)*nx + i + 1;
        double err = 1.0;

        
        //Variable                          Index range          Interior points
        //u_{n+1, i} = x0(i)                //0 ... nx           (1 ... nx-1)
        //l_{n+1, i} = x0(nx + 1 + i)       //nx+1  ... 2nx + 1  (nx+2 ... 2nx +1)
        //\pi_{n+1, i} = x0(2*nx + 2 + i)   //2nx+2 ... 3nx + 2 (2nx + 3 ... 3nx + 1)
        double temp_u, temp_ell, temp_pi;
        double temp_res = 0.0;
        int count = 0;
        while( (err > eps1 ) && (count < MAXITER)){
            //Periodic BC for U
            idxn = (n-1)*nx + nx ;
            idx0 = (n-1)*nx;

            u_vec(0) = u_vec(nx);
            u_vec(nx+1) =  u_vec(1);
            
            //Periodic BC for L
            ell_vec(0) =ell_vec(nx) - XMAX;
            ell_vec(nx+1) = ell_vec(1) + XMAX;
            
            //Periodic BC for \pi
            pi_vec(0) = pi_vec(nx);
            pi_vec(nx+1) = pi_vec(1);
            
            
            
            //Gauss-Seidel iteration
            for(int i = 1; i <= nx ; i++){
                idx1 = (n-1)*(nx+2) + i ;
                idx2 = (n-1)*(nx +2)+ i+ 1;
                idxu = i;
                idxl = i+ nx + 2;
                idxpi = i + 2*nx + 4;
                //u_n(i) = (dt1)*x0(i)- (dt1)*u[idx] - (dx2)*(x0(i+1) -2.0*x0(i) + x0(i-1));
                
                //U
                z[i] = (1.0 - omega)*z[i] +  omega*(1.0/(1.0 + dx2*2.0))*(-kappa  + dx2*(u_vec(i-1) + u_vec(i+1)) - one_over_four*dx1*(ell_vec(i+1) - ell_vec(i-1))*(pi_vec(i) +pi_var[idx1]));

                //L
                z[idxl] = (1.0 - omega)*z[idxl] + omega*(ell[idx1] - dt*one_over_four*u[idx1]*dx1*(ell[idx1+1] - ell[idx1-1]) - dt*one_over_four*dx1*(u_vec(i)*(ell_vec(i+1) - ell_vec(i-1))));

                //Pi
                z[idxpi] =  (1.0 - omega)*z[idxpi]  + omega*(pi_var[idx1]  - dt*one_over_four*pi_var[idx1+1]*(dx1*u_vec(i+1)) + dt*one_over_four*pi_var[idx1-1]*(dx1*u_vec(i-1))  +dt*one_over_four*pi_vec(i-1)*(dx1*u_vec(i-1)) - dt*one_over_four*pi_vec(i+1)*(dx1*u_vec(i+1)));

                
            }
            
            //Computing the residual
            
            temp_res = 0.0;
            for(int i = 1; i <= nx ; i++){
                idx1 = (n-1)*(nx+2) + i ;
                idx2 = (n-1)*(nx +2)+ i+ 1;
                idxu = i;
                idxl = i+ nx + 2;
                idxpi = i + 2*nx + 4;
                //u_n(i) = (dt1)*x0(i)- (dt1)*u[idx] - (dx2)*(x0(i+1) -2.0*x0(i) + x0(i-1));
                
                //U
                temp_u = kappa +  u_vec(i) - dx2*(u_vec(i-1)-a*u_vec(i) + u_vec(i+1)) + one_over_four*dx1*(ell_vec(i+1) - ell_vec(i-1))*(pi_vec(i) +pi_var[idx1]);
                
                //Ell
                temp_ell =  (ell_vec(i) - ell[idx1])  +  dt*one_over_four*u[idx1]*dx1*(ell[idx1+1] - ell[idx1-1]) + dt*one_over_four*dx1*(u_vec(i)*(ell_vec(i+1) - ell_vec(i-1)));

                //Pi
                temp_pi = -pi_vec(i)  +  pi_var[idx1]  - dt*one_over_four*pi_var[idx1+1]*(dx1*u_vec(i+1)) + dt*one_over_four*pi_var[idx1-1]*(dx1*u_vec(i-1))  +dt*one_over_four*pi_vec(i-1)*(dx1*u_vec(i-1)) - dt*one_over_four*pi_vec(i+1)*(dx1*u_vec(i+1));
            
                
                temp_res += 0.5*temp_u*temp_u +  0.5*temp_ell*temp_ell + 0.5*temp_pi*temp_pi;
                
                
                
            }
            
            err = sqrt(temp_res);
            
            count++;
            
        }
        
         //std::cout << "time iteration_" << n << " took " << count << " with error = " << err << std::endl;
    
        
        
    }
};

#endif /* section_h */
