//
//  Params.h
//  Hamiltonian_PSO
//
//  Created by Nader on 19/03/2017.
//  Copyright © 2017 Nader Ganaba. All rights reserved.
//

#ifndef Params_h
#define Params_h

#include <cmath>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <cstring>
#include <cstdlib>
#include <fstream> // ifstream
#include <sstream> // stringstream


namespace params_config {
    
    struct data: std::map <std::string, std::string>
    {
        // Here is a little convenience method...
        bool iskey( const std::string& s ) const
        {
            return count( s ) != 0;
        }
    };
    //---------------------------------------------------------------------------
    // The extraction operator reads configuration::data until EOF.
    // Invalid data is ignored.
    //
    std::istream& operator >> ( std::istream& ins, data& d )
    {
        std::string s, key, value;
        
        // For each (key, value) pair in the file
        while (std::getline( ins, s ))
        {
            std::string::size_type begin = s.find_first_not_of( " \f\t\v" );
            
            // Skip blank lines
            if (begin == std::string::npos) continue;
            
            // Skip commentary
            if (std::string( "#;" ).find( s[ begin ] ) != std::string::npos) continue;
            
            // Extract the key value
            std::string::size_type end = s.find( '=', begin );
            key = s.substr( begin, end - begin );
            
            // (No leading or trailing whitespace allowed)
            key.erase( key.find_last_not_of( " \f\t\v" ) + 1 );
            
            // No blank keys allowed
            if (key.empty()) continue;
            
            // Extract the value (no leading or trailing whitespace allowed)
            begin = s.find_first_not_of( " \f\n\r\t\v", end + 1 );
            end   = s.find_last_not_of(  " \f\n\r\t\v" ) + 1;
            
            value = s.substr( begin, end - begin );
            
            // Insert the properly extracted (key, value) pair into the map
            d[ key ] = value;
        }
        
        return ins;
    }
    
    //---------------------------------------------------------------------------
    // The insertion operator writes all configuration::data to stream.
    //
    std::ostream& operator << ( std::ostream& outs, const data& d )
    {
        data::const_iterator iter;
        for (iter = d.begin(); iter != d.end(); iter++)
            outs << iter->first << " = " << iter->second << std::endl;
        return outs;
    }
    
    
    
}

class Params{
public:
    double _Lmin, _Lmax;
    double _dt;
    int _nt;  //Size of population
    int _nx;      //Dimensions
    // int _epochs;
    int _max_iter;
    double _kappa;
    double _c, _omega;
    
    std::string filename;
    Params(std::string _filename);
    
    friend std::ostream& operator<<(std::ostream& os, const Params& pm);
    
};

Params::Params(std::string _filename){
    
    
    
    params_config::data myconfig;
    params_config::data::const_iterator iter;
    filename = _filename;
    std::string filename_1 = filename + ".ini";
    std::ifstream f(filename_1.c_str());
    
    f >> myconfig;
    f.close();
    
    //Lmin
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("L min" == iter->first){
            _Lmin  = std::stod (iter->second);
        }
    }
    //Lmax
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("L max" == iter->first){
            _Lmax  = std::stod (iter->second);
        }
    }

    
    //dt
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("dt" == iter->first){
            _dt = std::stod (iter->second);
        }
    }
    
    //kappa
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("kappa" == iter->first){
            _kappa = std::stod (iter->second);
        }
    }
    
    //c
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("c" == iter->first){
            _c = std::stod (iter->second);
        }
    }
    
    //omega
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("omega" == iter->first){
            _omega= std::stod (iter->second);
        }
    }
    
    //max iterations
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("max iterations" == iter->first){
            _max_iter = std::stoi (iter->second);
        }
    }
    
    //Number of time steps
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("Nt" == iter->first){
            _nt  = std::stoi (iter->second);
        }
    }
    
    //Number of grid poi ts
    for (iter = myconfig.begin(); iter != myconfig.end(); iter++){
        if("Nx" == iter->first){
            _nx = std::stoi (iter->second);
        }
    }
    
}

std::ostream& operator<<(std::ostream& os, const Params& pm)
{
    
    os << "Parameters:\n";
    os << "Position limits = [" <<pm._Lmin << ", " << pm._Lmax << std::endl;
    os << "Grid size = [" << pm._nt << ", " << pm._nx << std::endl;
    os << "Dt = " << pm._dt << std::endl;
    os << "Max iterations = " << pm._max_iter << std::endl;
    os << "Kappa = " << pm._kappa << std::endl;
    os << "Omega = " << pm._omega << std::endl;
    os << "C = " << pm._c << std::endl;
    return os;
}


#endif /* Params_h */

