//
//  main.cpp
//  Multisymplectic_GS2
//  This is an implementation of multisymplectic variational integrator

//  Created by Nader on 21/06/2017.
//  Copyright � 2017 Nader Ganaba. All rights reserved.
//  This code is a prototype for Multisymplectic code. The purpose is just testing at this stage.
//  This version of the code uses Gauss-Seidel method for solving the system of nonlinear equations, instead of relying on GSL, Eigen or Boost. The two main advantages for GS method is that it runs faster than the other codes and furthermore it makes the code parallelisable.


#include<iostream>
#include<algorithm>
#include<iomanip>
#include<fstream>
#include<sstream>
#include<string>
#include<cstring>
#include<cctype>
#include<vector>
#include<cmath>

#include "utils.h"
#include "basespace.h"
#include "section.h"
#include <time.h>


int main(int argc, const char * argv[]) {
    // insert code here...
    double time = 0.0;
    
    
    clock_t start = clock();
    
    //multisymplectic_CH( );
    //  testSection_ch();
    multisymplectic_CH();
    clock_t stop = clock();
    double elapsed = (double)(stop - start)/ CLOCKS_PER_SEC;
    std::cout << "sim_end in " << elapsed << " seconds \n" ;
    
    return 0;
}
