//
//  basespace.h
//  Multisymplectic_GS
//
//  Created by Nader on 09/03/2017.
//  Copyright © 2017 Nader Ganaba. All rights reserved.
//

#ifndef basespace_h
#define basespace_h


#include<vector>
#include <stdio.h>
#include <stdlib.h>

struct section_params
{
    double dx;
    double dt;
    int nx;
    int nt;
    double x_max;
};


class BaseSpace{
    
private:
    
    int total_dims;
    int space_dims;
    
    int num_time_points;
    int num_space_points;
    int total_points;
    double dt;
    double dx;
    
    double T_end;
    double x_min;
    double x_max;
    
    double* space_axis;
    double* time_axis;
public:
    
    typedef struct{
        double t; double x;
    } BasePoint;
    
    BaseSpace(int _nt, int _nx,  double _tend, double _xmin, double _xmax){
        num_time_points = _nt;
        T_end = _tend;
        
        dt = (T_end) / (double)( num_time_points - 1);
        
        
        num_space_points = _nx;
        x_min = _xmin;
        x_max = _xmax;
        
        dx = (x_max -x_min) / (double) ( num_space_points - 1);
        total_points = num_space_points*num_time_points;
        
        time_axis = (double*) calloc (num_time_points,sizeof(double));
        space_axis = (double*) calloc (num_space_points,sizeof(double));
        //time_axis.resize(num_time_points);
        
        for(int i = 0; i < num_time_points; i++){
            time_axis[i] = i*dt;
        }
        
        for(int i = 0; i < num_space_points; i++){
            space_axis[i] = x_min + i*dx;
        }
        
        std::cout << "Done numering" << std::endl;
        
        
    }
    
    int get_num_t_points(){ return num_time_points;}
    int get_num_x_points(){ return num_space_points;}
    int get_total_points(){return total_points;}
    BasePoint get_location(int i, int j) { BasePoint point; point.t = time_axis[i]; point.x = space_axis[j]; return point;}
    
    double time_at(int i) {return time_axis[i];}
    double x_at(int i ) {return space_axis[i];}
    
};


#endif /* basespace_h */
