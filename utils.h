//
//  utils.h
//  Multisymplectic_GS
//
//  Created by Nader on 09/03/2017.
//  Copyright © 2017 Nader Ganaba. All rights reserved.
//

#ifndef utils_h
#define utils_h




#include<iostream>
#include<cstdlib>
#include<algorithm>
#include<iomanip>
#include<fstream>
#include<sstream>
#include<string>
#include<cstring>
#include<cctype>
#include<vector>
#include<cmath>
#include <complex>
#include "basespace.h"
#include "section.h"

void testLaplacian_ch(){
    
    int nx = 128, nt = 10;
    Section_CH u(nx, nt);
    
    u.set_init_cond();
    
}




//
//
//
//
void multisymplectic_CH(){
    
    
    
    int nx = 128, nt = 402;
    std::cout <<"1\n";
    Section_CH u(nx, nt);
    std::cout <<"2\n";
    int nt_test = 401;
    std::cout <<"3\n";
    u.set_init_cond();
    std::cout <<"4\n";
    int status;
    size_t i, iter = 0;
    
    const size_t n_size = 3*(nx + 2);
    
    // struct section_params p = u.load_params();
    
    
    // gsl_vector *u_n = gsl_vector_alloc (n_size);
    //
    //u.set_curr_iter(1);
    //
    //u.Section_CH_guess(0);
    
    std::cout <<"5\n";
       for(int i_time = 1; i_time < nt_test ; i_time++){
           // int i_time = 1;
            std::cout << "time step = " << i_time << std::endl;
            u.set_curr_iter(i_time);
            u.Section_CH_guess(i_time);
    // u.print_z(i_time);
             u.eval_EL_CH( i_time);
             u.update_Section_CH(i_time);
           
             u.comp_conserved_m(i_time);
    }
   // u.print_z(2);
    //
    //    std::cout << "The solution is = " << x << std::endl;
    
    // std::cout << "The value = " << (u.eval_EL_heat(1,x).squaredNorm() )<< std::endl;
    // u.print_u(nt_test-1);
    // u.print_l(nt_test-1);
    // u.print_vars(nt_test-1);
    //u.print_c();
    //u.export_u(nt_test-1, "test_plot_ch.dat");
    
    std::cout << "CH test end\n";
    
    
}

//void multisymplectic_CH( ){
////
//    int nx = 128, nt = 330;
//    Section_CH u(nx, nt);
//
//    int nt_test = 321;
//    u.set_init_cond();
//
//
//    //    u.test_advect_ell(1);
//    //  u.export_u(0, "init_plot_kdv_60_65.dat");
//    //u.set_init_cond();
//    //
//    //
//
//   // Eigen::MatrixXf R = Eigen::MatrixXf::Identity(3*(nx + 2), 3*(nx + 2));
//
//
//    gsl_vector *d = gsl_vector_alloc (3*(nx + 2));
//    gsl_vector *x = gsl_vector_alloc (3*(nx + 2));
//    gsl_vector *y = gsl_vector_alloc (3*(nx + 2));
//    gsl_vector_set_all(x, 1.0);
//
//
//    //  x = u.Section_CH_guess(0);
//    //x(0) = 0.0;
//    // x(nx-1) = 0.0;
//
//    // std::cout << "The solution is = \n" << x << std::endl;
//
//    //   u.comp_conserved_m(0);
//
//    double eps1 = 5e-9;
//    double eps2= 5e-9;
//
//    double err = 1;
//
//    double tol = eps2;
//    int Max_iter = 100;
//    double temp = 0.0;
//    double ferr = 0.0;
//    int count = 0;
//    std::cout << "Multisymplectic CH" << "\t max iter = " << Max_iter << std::endl;
//
//
//    for(int i_time = 1; i_time < nt_test ; i_time++){
//        std::cout << "time step = " << i_time << std::endl;
//        err = 1;
//        count = 0;
//
//
//        u.Section_CH_guess(i_time-1, x);
//
//        //x = Eigen::VectorXf::Constant(3*(nx + 2),1.0);
//        while( (err > tol) && (count < Max_iter)){
//            //std::cout << "Broyden test\n";
//            std::cout << count;
//
//            u.eval_EL_CH(i_time, x, y);
//
//            R = Jacobian_cmpxstep_ch(u, x, i_time);
//
//            //R = u.Jacobian(i_time, x);
//            d = R.fullPivHouseholderQr().solve(-y);
//            //d = R.fullPivLu().solve(-y);
//            x = x + d;
//            temp = (u.eval_EL_CH(i_time,x)).squaredNorm();
//            err = temp/2.0;
//
//            std::cout << "\t " << err << std::endl;
//            count++;
//        };
//
//        u.update_Section_CH(i_time,x);
//
//        // u.comp_conserved_m(i_time);
//    }
//    //
//    //    std::cout << "The solution is = " << x << std::endl;
//
//    // std::cout << "The value = " << (u.eval_EL_heat(1,x).squaredNorm() )<< std::endl;
//    // u.print_u(nt_test-1);
//    // u.print_l(nt_test-1);
//    u.print_vars(nt_test-1);
//    //u.print_c();
//    u.export_u(nt_test-1, "test_plot_ch.dat");
//
//    gsl_vector_free (x);
//    gsl_vector_free (d);
//    gsl_vector_free (y);
//    std::cout << "CH test end\n";
//}
//

#endif /* utils_h */
